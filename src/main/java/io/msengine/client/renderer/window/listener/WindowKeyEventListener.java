package io.msengine.client.renderer.window.listener;

public interface WindowKeyEventListener {
	
	void windowKeyEvent(int key, int scancode, int action, int mods);
	
}
