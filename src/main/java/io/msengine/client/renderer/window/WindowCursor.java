package io.msengine.client.renderer.window;

public enum WindowCursor {
	
	ARROW,
	TEXT,
	CROSSHAIR,
	HAND,
	HRESIZE,
	VRESIZE;
	
}
