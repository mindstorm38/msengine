package io.msengine.common.resource.metadata;

import java.io.InputStream;

public interface Metadatable {
	
	public InputStream getMetadataInputStream();
	
}
