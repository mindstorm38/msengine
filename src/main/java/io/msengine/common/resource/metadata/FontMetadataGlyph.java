package io.msengine.common.resource.metadata;

public class FontMetadataGlyph {
	
	public final int x;
	public final int y;
	public final int width;
	
	public FontMetadataGlyph(int x, int y, int width) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		
	}
	
}
