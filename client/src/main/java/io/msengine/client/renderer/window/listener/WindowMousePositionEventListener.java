package io.msengine.client.renderer.window.listener;

public interface WindowMousePositionEventListener {

	void windowMousePositionEvent(int x, int y);
	
}
