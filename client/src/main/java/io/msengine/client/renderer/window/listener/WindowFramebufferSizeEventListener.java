package io.msengine.client.renderer.window.listener;

public interface WindowFramebufferSizeEventListener {
	
	void windowFramebufferSizeChangedEvent(int width, int height);
	
}
