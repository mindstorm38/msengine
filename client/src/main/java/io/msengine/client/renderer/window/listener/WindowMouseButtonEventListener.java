package io.msengine.client.renderer.window.listener;

public interface WindowMouseButtonEventListener {
	
	void windowMouseButtonEvent(int button, int action, int mods);
	
}
