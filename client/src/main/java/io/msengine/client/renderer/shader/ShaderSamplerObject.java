package io.msengine.client.renderer.shader;

/**
 * An interface that own a sample-able object, like textures, or framebuffers.
 */
public interface ShaderSamplerObject {

	int getSamplerId();
	
}
