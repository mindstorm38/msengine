package io.msengine.client.renderer.window.listener;

public interface WindowScrollEventListener {
	
	void windowScrollEvent(double xoffset, double yoffset);
	
}
