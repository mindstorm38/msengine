package io.msengine.client.renderer.window.listener;

public interface WindowCharEventListener {
	
	void windowCharEvent(char codepoint);
	
}
