package io.msengine.common.util.math;

public interface RectBoundingBox {

	float[] getCorners();
	
}
